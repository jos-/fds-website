---
title: federatief.datastelsel.nl
---

{{< blocks/cover title="Welcome at the federed data space of The Netherlands!" image_anchor="top" height="full" >}}
<a class="btn btn-lg btn-primary me-3 mb-4" href="/nl/">
  Dutch version <i class="fas fa-arrow-alt-circle-right ms-2"></i>
</a>
<a class="btn btn-lg btn-info me-3 mb-4" href="https://realisatieibds.pleio.nl/" target="_blank">
  Interbestuurlijke Datastrategie <i class="fa fa-globe ms-2 "></i>
</a>
<p class="lead mt-5">Only a Dutch version of this site is currently available. We would like to be inclusive and open and therefor share what we do, know and develop in English too ... but this is still work in progress</p>
{{< /blocks/cover >}}
