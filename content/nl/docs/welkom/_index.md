---
title: Welkom
weight: 10
# menu: {main: {weight: 20}}
categories: 
description: >
  Hoe werkt dit platform en hoe kun je bijdragen?  
  Hier vind je de wegwijzer en instructies om bij te dragen 
---

Welkom bij federatief.datastelsel.nl, het platform voor ons allemaal!

Het federatief datastelsel is een open source project dat voor iedereen toegankelijk is om te
gebruiken en te verbeteren. De ontwikkeling hiervan doen we juist graag met elkaar, dus we kijken
uit naar je betrokkenheid.

We leggen je graag uit hoe we met elkaar willen samenwerken. In [strategie van samenwerken](/docs/welkom/strategie-van-samenwerken/) staat hoe we de samenwerking voor ons zien. Bij [werkomgeving](/docs/welkom/werkomgeving/) hopen we jullie inzicht en duidelijkheid te geven in hoe we  die samenwerking dan het liefste willen realiseren. 

Heb je vragen? Gaan dan direct naar  MatterMost (onze chat)! Bij het kanaal algemeen in MatterMost kun je al je vragen stellen, mocht je dan willen aanhaken bij een specifiek thema dan zorgen we dat je geattendeerd wordt op het juiste kanaal!



