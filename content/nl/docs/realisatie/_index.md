---
title: Realisatie
# menu: {main: {weight: 30}}
weight: 40
categories: 
tags: [docs]
description: >
  Hier vind je alle verdiepende documenten die inzicht geven in diverse onderwerpen zoals het [toekomstbeeld
  van het federatief datastelsel](/docs/realisatie/toekomstbeeld/), de 
  [capabilities](/docs/realisatie/capabilities/), de architectuurlagen en
  de [besluiten](/docs/realisatie/besluiten/) die genomen zijn voor de inrichting van het federatief datastelsel 
---