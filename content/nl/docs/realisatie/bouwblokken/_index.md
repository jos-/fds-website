---
title: Bouwblokken
weight: 40
description: >
  De bouwblokken, ontwerpstructuren, werkpakketten, componenten die de realisatie van
  de [capabilities](/docs/realisatie/capabilities/) vormen.
---
