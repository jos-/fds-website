---
title: Uitwisseling API's
tag: bouwblok
categories: [Interoperabiliteit, Dataservices]
description: >
  Bouwblok Uitwisseling API's.
  Valt onder capability [Interoperabiliteit | Dataservices](/docs/realisatie/capabilities/interoperabiliteit/dataservices/).
---

Twee participanten wisselen informatie uit in FDS op basis van API's. Daarvoor stelt de aanbiedende
participant een API beschikbaar die door de vragende participant rechtstreeks benaderd wordt.
Hiermee wordt volledig voldaan aan het principe [data bij de
bron](/docs/realisatie/toekomstbeeld/data-bij-de-bron/).  De uitwisseling vindt plaats op basis van
vooraf overeengekomen certificaten welke bij elke transactie gecontroleerd worden (dit is
overeenkomstig de <a href="https://nlx.io/" target="_blank">NLx</a> aanpak). Dit laatste is niet in
het onderstaande model opgenomen.

<!--- 

Hier moeten we nog iets mee

Een voorbeeld van
een bouwsteen voor gegevensinteroperabiliteit die een gemeenschappelijke API voor gegevensuitwisseling biedt, is de ‘Context’
Broker’ van de Connecting Europe Facility (CEF)50, aanbevolen door de Europeaan
Commissie voor het delen van right-time data tussen meerdere organisaties.

--> 

![API-view](images/api-view.png)

## Links

- [...]
