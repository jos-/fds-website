---
title: Self-description databron
weight: 2
date: 2023-10-04
tags:
categories: [Interoperability, Dataservices]
description: >
  Met een Self-description van de databron kan een bronhouder _zelf_ beschrijven hoe
  deze databron eruit ziet, terwijl anderen daar gebruik van kunnen maken 
---

Afspraken over formaten spelen in FDS, als afspraken stelsel, een belangrijke rol.  Ze vormen deels
het fundament onder de strategie van FDS: het laten ontstaan van een afsprakenstelsel. Een
belangrijke afspraak is dat Databronnen via een self-description aan het stelsel angeboden worden.
Het formaat van die self-description ligt vast in het [self-description formaat van een databron](#).
Daarnaast kan ook ook de datakwaliteit van de bron vastgelegd worden en ook dat vindt plaats via een
[self-description](#). Het vastleggen van deze informatie vindt plaats door de FDS-participant. Er
vindt een beoordeling plaats van de aangeboden bron door de FDS-poortwachter. Die beoordeling vindt
deels geautomatiseerd plaats, via de Beoordelingsvoorziening, en is deels een beoordeling door de
poortwachter zelf. De beschrijvingen vand e databronnen en de beschrijvingen van de bijbehorende
datakwaliteit worden opgeslagen in een opslagvoorziening.

![ABB databron formaten](images/abbssfmt.png)

### Revisieafspraken

>>> Dit gaat naar een ander hoofdstuk

Voor alle formaten die binnen FDS voorgeschreven worden geldt dat er
een actueel formaat is, en dat de voorgaande versie van het
voorgeschreven formaat geldt gedurende de periode (de grace period)
die aangegeven wordt hierboven.  Deelname aan FDS betekent dat de
participanten zich verplichten in de grace period hun aanbod en
systemen om te stellen naar de nieuwe versie. Voor deze omstelling
kunnen geen kosten in rekening gebracht worden. Het FDS team is,
wanneer noodzakelijk, beschikbaar voor inhoudelijk advies ten aanzien
van de omstelling.
