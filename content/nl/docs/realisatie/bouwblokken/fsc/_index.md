---
title: FSC
description: >
  Federatieve Service Connectiviteit Standaard
---

## Beschrijving

Meer info is te vinden op de website van [CommonGround/FSC](https://commonground.nl/cms/view/736309a1-739a-47fc-abfd-67e71f1d9e59/consultatie-fsc)

## Architectuurlagen

### Grondslagenlaag

Hier vind je wetten en drivers.

Componenttypen die we hier verwachten:

- Doel/Driver
- Principe
- Requirement
- Afspraak/Standaard (Wet/Overeenkomst)

### Organisatorische laag

De organisatorische componenten vind je hier.

Componenttypen die we hier verwachten:

- (Actor)
- Rol
- Functie
- Service
- Afspraak/Standaard
- (Contract)

### Informatielaag

Componenttypen die we hier verwachten:

- Metadata-object
- Metadata-object-relatie
- Afspraak/Standaard

### Applicatielaag

Componenttypen die we hier verwachten:

- Functie (Technische stelselfuncties)
- Applicatieservice
- Afspraak/Standaard

### Technologielaag

Componenttypen die we hier verwachten:

- Node
- Netwerk
- Afspraak/Standaard
