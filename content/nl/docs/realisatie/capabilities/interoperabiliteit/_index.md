---
title: Interoperabiliteit
weight: 2
date: 2023-10-04
categories: [Capabilities, Interoperabiliteit]
tags: 
description: >
  Gegevensinteroperabiliteit, met betrekking tot aspecten zoals gegevensuitwisseling API's,
  formaten voor gegevensrepresentatie, evenals herkomst en traceerbaarheid

---
 
Hoofdcategorie bouwblokken, namelijk **Interoperabiliteit**!

{{< capabilities-diagram selected="interoperabiliteit" >}}

Data delen binnen FDS wordt mogelijk gemaakt door dat we afspraken vastleggen over hoe we data
beschrijven en hoe we data kunnen bereiken. Verder kunnen we de herkomst en het gebruik van data
inzichtelijk maken. We maken daarbij gebruik van gangbare standaarden, waarop we soms een specifiek
FDS profiel ontwikkelen. Dat laatste vormt het hart van de FDS-specifieke capability 'ontwikken en
beheren meta-data standaarden'. 
