---
title: Modellen
weight: 1
date: 2023-10-17
categories: [Capabilities, Interoperabiliteit, Modellen]
tags: 
description: >
  Interoperabiliteit | Modellen: Data- en informatiemodellen en -formaten
---

{{< capabilities-diagram selected="modellen" >}}

## Beschrijving

Deze capability stelt een gemeenschappelijk formaat vast voor specificaties voor gegevensmodel en
weergave van gegevens in de payloads van gegevensuitwisseling. Gecombineerd met de [Interoperabiliteit | Dataservices](../dataservices/),
zorgt dit voor volledige interoperabiliteit bij deelnemers.

## Bouwblokken

- [...]
