---
title: Traceerbaarheid
weight: 3
date: 2023-10-17
categories: [Capabilities, Interoperabiliteit, Traceerbaarheid]
tags: 
description: >
  Interoperabiliteit | Traceerbaarheid: Traceerbaarheid en herleidbaarheid
---

{{< capabilities-diagram selected="traceerbaarheid" >}}

## Beschrijving

Deze capability biedt de middelen voor tracering en tracking in het proces van dataverstrekking en
dataverbruik/gebruik. Het biedt daardoor de basis voor een aantal belangrijke functies, van
identificatie van de lijn van gegevens tot auditbestendige logboekregistratie van transacties. Het
maakt ook de implementatie van een breed scala aan tracking use cases op applicatieniveau mogelijk,
zoals het volgen van producten of materiaalstromen in een supply chain.

## Bouwblokken

- [...]
