---
title: Governance
weight: 1
date: 2023-09-13
categories: [Capabilities, Governance]
tags: 
description: >
  Governance capabilities zijn artefacten die de zakelijke relaties tussen alle rollen regelen.
---

{{< capabilities-diagram selected="governance" >}}
