---
title: Bestuurlijk
weight: 1
date: 2023-10-17
categories: [Capabilities, Governance, Bestuurlijk]
tags: 
description: >
  Governance | Bestuurlijk: Bestuurlijke overeenkomsten en juridische constructen
---

{{< capabilities-diagram selected="bestuurlijk" >}}

## Beschrijving

Bestuurlijke overeenkomsten en juridische constructen.

Alle deelnemers aan het federatief datastelsel of een dataspace moeten het eens zijn over bepaalde
functionele, technische, operationele en juridische aspecten. Hoewel sommige overeenkomsten op een
generieke of sectorspecifieke manier herbruikbaar zijn (bijv. Regelboeken), zijn anderen
gebruiksspecifiek.

## Bouwblokken

- [...]
