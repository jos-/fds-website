---
title: Operationeel
weight: 1
date: 2023-10-17
categories: [Capabilities, Governance, Operationeel]
tags: []
description: >
  Governance | Operationeel: Operationele overeenkomsten en afspraken
---

{{< capabilities-diagram selected="operationeel" >}}

## Beschrijving

Operationele overeenkomsten reguleren beleid dat moet worden afgedwongen tijdens de dataspace
operatie. Ze omvatten bijvoorbeeld voorwaarden die betrekking hebben op het steeds groter wordende
belang van naleving van verplichte voorschriften zoals GDPR of de 2e Payment Services Richtlijn
(PSD2) in de financiële sector.

## Bouwblokken

- [...]
