---
title: Beheer
weight: 3
date: 2023-10-17
categories: [Capabilities, Governance, Beheer]
tags: 
description: >
  Governance | Beheer: Continuïteit en doorontwikkeling van het stelsel
---

{{< capabilities-diagram selected="beheer" >}}

## Beschrijving

Continuïteit en doorontwikkeling van het stelsel ... Regie van het stelsel, etc, etc

Het continuïteitsmodel beschrijft de processen voor het beheer van wijzigingen, versies en releases
voor normen en overeenkomsten. Dit omvat ook het bestuursorgaan voor besluitvorming en
conflictoplossing.

## Bouwblokken

- [...]
