---
title: Datawaarde
weight: 4
date: 2022-11-02
categories: [Capabilities, Datawaarde]
tags: 
description: >
  Gegevenswaardecreatie, met betrekking tot aspecten zoals publicatie van gegevensaanbiedingen,
  ontdekking van dergelijke aanbiedingen op basis van metadata en
  gegevenstoegang/gebruiksboekhouding, die essentieel zijn om gegevens als een economisch asset
  te verwerken
---

{{< capabilities-diagram selected="datawaarde" >}}
