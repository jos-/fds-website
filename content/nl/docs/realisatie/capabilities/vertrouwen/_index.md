---
title: Vertrouwen
weight: 3
date: 2022-10-25
categories: [Capabilities, Vertrouwen]
tags: 
description: >
  Gegevenssoevereiniteit, met betrekking tot aspecten zoals identiteitsbeheer, betrouwbaarheid
  van deelnemers, evenals gegevenstoegang en gebruikscontrole

---

{{< capabilities-diagram selected="vertrouwen" >}}
