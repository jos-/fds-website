---
title: Veiligheid
weight: 3
date: 2023-10-17
categories: [Capabilities, Vertrouwen, Veiligheid]
tags: 
description: >
  Vertrouwen | Veiligheid: Vertrouwde uitwisseling
---

{{< capabilities-diagram selected="veiligheid" >}}

## Beschrijving

Deze capability faciliteert de vertrouwde data-uitwisseling onder deelnemers, waardoor deelnemers in
een data-uitwisselingstransactie geruststellen dat die andere deelnemers echt zijn wie zij beweren
te zijn en dat zij voldoen aan gedefinieerde regels/overeenkomsten. Dit kan worden bereikt door
organisatorische maatregelen (bijv. certificering of geverifieerde referenties) of technische
maatregelen (bijvoorbeeld attestatie op afstand).

## Bouwblokken

- [...]
