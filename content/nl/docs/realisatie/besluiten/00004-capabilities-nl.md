---
title: 00004 Capabilities NL
date: 2023-11-07
author: Peter Bergman, Mike Dell, Marc van Andel, Gert-Jan Aaftink
categories: [DR]
tags: 
status: proposed
description: >
   We gebruiken de OpenDEI building blocks als capabilities,
   maar hoe vertalen we deze naar het Nederlands?
---

## Context en probleemstelling

Gegeven de context van de DR [00001 Basisstructuur](./00001-basisstructuur/) waarin we bepalen dat we
met capabilities gaan werken volgens de building blocks van OpenDEI, hoe noemen we de capabilities
dan in het Nederlands? Nemen we de OpenDEI building blocks letterlijk over? Geven we nog een andere
context aan onze capabilities? (Waarmee ook de vertaling naar het Engels anders wordt?)

## Beslissingsfactoren <!-- optional -->

- Context:
  - Doelstellingen [Realisatie Interbestuurlijke Datastrategie
    (IBDS)](https://realisatieibds.pleio.nl/)
  - Doelstellingen [Federatief Datastelsel
    (FDS)](https://realisatieibds.pleio.nl/cms/view/8852ee2a-a28a-4b91-9f3e-aab229bbe07f/federatief-datastelsel)
- Decision Records:
  - DR [00001 Basisstructuur](../00001-basisstructuur/)
- OpenDEI:

## Overwogen opties

- [OpenDEI oorspronkelijk](#opendei-oorspronkelijk)
- [OpenDEI letterlijke vertaling](#opendei-letterlijke-vertaling)
- [Mike's vertaling](#mikes-vertaling)
- [Peter's vertaling](#peters-vertaling)
- [Capabilities sessies](#capabilities-sessies)

## Besluit

We kiezen voor een zo kort en eenvoudig mogelijke duiding en daarom nemen we voornamelijk [Mike's
vertaling](#mikes-vertaling) over. Daarbij voegen we wel een zin per capability / woord toe om te
zorgen voor een juiste en gedeelde 'lading' van elke capability.

- **Governance**
  - **Bestuurlijk**: Bestuurlijke overeenkomsten en juridische constructen
  - **Operationeel**: Operationele overeenkomsten en afspraken
  - **Beheer**: Continuïteit en doorontwikkeling van het stelsel
- **Interoperabiliteit**
  - **Modellen**: Data- en informatiemodellen en -formaten
  - **Dataservices**: API's voor data uitwisseling
  - **Traceerbaarheid**: Traceerbaarheid en herleidbaarheid
- **Datawaarde**
  - **Metadata**: Metadata in brede zin met beschrijvingen, verwijzingen en meer
  - **Verantwoording**: Verantwoording van datagebruik
  - **Publicatie**: Publicatie- en marktplaatsdiensten
- **Vertrouwen**
  - **Identiteit**: Identiteitsbeheer
  - **Toegang**: Toegangscontrole en gebruikscontrole
  - **Veiligheid**: Vertrouwde uitwisseling

### Positieve gevolgen <!-- optional -->

- … <!-- numbers of options can vary -->

### Negatieve Consequences <!-- optional -->

- … <!-- numbers of options can vary -->

## Pros en Cons van de oplossingen <!-- optional -->

### OpenDEI oorspronkelijk

- **Governance**
  > Business building blocks are artifacts that regulate the business relationships between all
  > roles.
  - **Overarching cooperation agreement**
    > All data space participants need to agree on certain functional, technical, operational and
    > legal aspects. While some agreements are reusable in a generic or sector-specific way (e. g.
    > rule books), others are use-case specific.
  - **Operational (e.g. SLA)**
    > Operational agreements regulate policies that need to be enforced during data space operation.
    > For example, they comprise terms and conditions dealing with the ever-growing importance of
    > compliance with mandatory regulations like GDPR or the 2nd Payment Services Directive (PSD2)
    > in the finance sector.
  - **Continuity model**
    > The Continuity Model describes the processes for the management of changes, versions, and
    > releases for standards and agreements. This also includes the governance body for
    > decision-making and conflict resolution.
- **Interoperability**
  > Data interoperability, covering aspects such as data exchange APIs, data representation formats,
  > as well as provenance and tracebility
  - **Data models and formats**
    > This building block establishes a common format for data model
    specifications and representation of data in data exchange payloads. Combined with the Data
    Exchange APIs building block, this ensures full interoperability among participants
  - **Data exchange APIs**
    > This building block facilitates the sharing and exchange of data (i.e., data provision and
    > data consumption/use) between data space participants. An example of a data interoperability
    > building block providing a common data exchange API is the ‘Context Broker’ of the Connecting
    > Europe Facility (CEF)50, which is recommended by the European Commission for sharing
    > right-time data among multiple organisations.
  - **Provenance and traceability**
    > This building block provides the means for tracing and tracking in the process of data
    > provision and data consumption/use. It thereby provides the basis for a number of important
    > functions, from identification of the lineage of data to audit-proof logging of transactions.
    > It also enables implementation of a wide range of tracking use cases at application level,
    > such as tracking of products or material flows in a supply chain.
- **Data value**
  > Data value creation, covering aspects such as publication of data offerings, discovery of such
  > offerings based on metadata, and data access/usage accounting, which are essential to handle
  > data as an economic asset
  - **Metadata and Discovery Protocol**
    > This building block incorporates publishing and discovery mechanisms for data resources and
    > services, making use of common descriptions of resources, services, and participants. Such
    > descriptions can be both domain-agnostic and domain-specific. They should be enabled by
    > semantic-web technologies and include linked-data principles.
  - **Data usage accounting**
    > This building block provides the basis for accounting access to and/or usage of data by
    > different users. This in turn is supportive of important functions for clearing, payment, and
    > billing (including data-sharing transactions without involvement of data marketplaces).
  - **Publication and marketplace services**
    > To support the offering of data resources and services under defined terms and conditions,
    > marketplaces must be established. This building block supports publication of these offerings,
    > management of processes linked to the creation and monitoring of smart contracts (which
    > clearly describe the rights and obligations for data and service usage), and access to data
    > and services.
- **Trust**
  > Data sovereignty, covering aspects such as identity management, trustworthiness of participants,
  > as well as data access and usage control
  - **Identity management**
    > The IM building block allows identification, authentication, and authorisation of stakeholders
    > operating in a data space. It ensures that organisations, individuals, machines, and other
    > actors are provided with acknowledged identities, and that those identities can be
    > authenticated and verified, including additional information provisioning1, to be used by
    > authorisation mechanisms to enable access and usage control. The IM building block can be
    > implemented on the basis of readily available IM platforms that cover parts of the required
    > functionality. Examples of open-source solutions are the KeyCloak infrastructure, the Apache
    > Syncope IM platform, the open-source IM platform of the Shibboleth Consortium, or the FIWARE
    > IM framework. Integration of the IM building block with the eID building block of the
    > Connecting Europe Facility (CEF)55, supporting electronic identification of users across
    > Europe, would be particularly important. Creation of federated and trusted identities in data
    > spaces can be supported by European regulations such as EIDAS.
  - **Access and usage control / policies**
    > This building block guarantees enforcement of data access and usage policies defined as part
    > of the terms and conditions established when data resources or services are published (see
    > ‘Publication and Services Marketplace’ building block below) or negotiated between providers
    > and consumers. A data provider typically implements data access control mechanisms to prevent
    > misuse of resources, while data usage control mechanisms are typically implemented on the data
    > consumer side to prevent misuse of data. In complex data value chains, both mechanisms are
    > combined by prosumers. Access control and usage control rely on identification and
    > authentication.
  - **Trusted exchange**
    > This building block facilitates trusted data exchange among participants, reassuring
    > participants in a data exchange transaction that other participants really are who they claim
    > to be and that they comply with defined rules/agreements. This can be achieved by
    > organisational measures (e.g. certification or verified credentials) or technical measures
    > (e.g. remote attestation).

### OpenDEI letterlijke vertaling

(vertaald mbv Google Translate)

- **Bestuur**
  > Governance capabilities zijn artefacten die de zakelijke relaties tussen alle rollen regelen.
  - **Overkoepelend samenwerkingsakkoord**
    > Alle deelnemers aan de dataspace moeten het eens zijn over bepaalde functionele,
    > technische, operationele en juridische aspecten. Hoewel sommige overeenkomsten op een
    > generieke of sectorspecifieke manier herbruikbaar zijn (bijv. Regelboeken), zijn anderen
    > gebruiksspecifiek.
  - **Operationeel (bijv. SLA)**
    > Operationele overeenkomsten reguleren beleid dat moet worden afgedwongen tijdens de dataspace
    > operatie. Ze omvatten bijvoorbeeld voorwaarden die betrekking hebben op het steeds groter
    > wordende belang van naleving van verplichte voorschriften zoals GDPR of de 2e Payment Services
    > Richtlijn (PSD2) in de financiële sector.
  - **Continuïteitsmodel**
    > Het continuïteitsmodel beschrijft de processen voor het beheer van wijzigingen, versies en
    > releases voor normen en overeenkomsten. Dit omvat ook het bestuursorgaan voor besluitvorming
    > en conflictoplossing.
- **Interoperabiliteit**
  > Gegevensinteroperabiliteit, met betrekking tot aspecten zoals gegevensuitwisseling API's,
  > formaten voor gegevensrepresentatie, evenals herkomst en traceerbaarheid
  - **Datamodellen en formaten**
    > Deze bouwsteen stelt een gemeenschappelijk formaat vast voor specificaties voor gegevensmodel
    > en weergave van gegevens in de payloads van gegevensuitwisseling. Gecombineerd met de Building
    > Builder van de Data Exchange API's, zorgt dit voor volledige interoperabiliteit bij deelnemers
  - **API's voor gegevensuitwisseling**
    > Deze bouwsteen vergemakkelijkt het delen en uitwisselen van gegevens (d.w.z.
    > gegevensvoorziening en dataconsumptie/gebruik) tussen gegevensruimte -deelnemers. Een
    > voorbeeld van een bouwsteen voor gegevensinteroperabiliteit die een gemeenschappelijke
    > gegevensuitwisseling API biedt, is de ‘contextmakelaar’ van de Connecting Europe Facility
    > (CEF), die door de Europese Commissie wordt aanbevolen voor het delen van juiste gegevens
    > over meerdere organisaties.
  - **Herkomst en traceerbaarheid**
    > Deze bouwsteen biedt de middelen voor tracering en tracking in het proces van
    > gegevensvoorziening en gegevensverbruik/gebruik. Het biedt daardoor de basis voor een aantal
    > belangrijke functies, van identificatie van de lijn van gegevens tot auditbestendige
    > logboekregistratie van transacties. Het maakt ook de implementatie van een breed scala aan
    > tracking -use cases op applicatieniveau mogelijk, zoals het volgen van producten of
    > materiaalstromen in een supply chain.
- **Gegevenswaarde**
  > Gegevenswaardecreatie, met betrekking tot aspecten zoals publicatie van gegevensaanbiedingen,
  > ontdekking van dergelijke aanbiedingen op basis van metadata en
  > gegevenstoegang/gebruiksboekhouding, die essentieel zijn om gegevens als een economisch asset
  > te verwerken
  - **Metagegevens en ontdekkingsprotocol**
    > Deze bouwsteen omvat publicatie- en ontdekkingsmechanismen voor gegevensbronnen en -services,
    > waarbij gebruik wordt gemaakt van gemeenschappelijke beschrijvingen van bronnen, diensten en
    > deelnemers. Dergelijke beschrijvingen kunnen zowel domein-agnostisch als domeinspecifiek zijn.
    > Ze moeten worden ingeschakeld door semantische-WEB-technologieën en bevatten principes met
    > gekoppelde gegevens.
  - **Accounting van gegevensgebruik**
    > Deze bouwsteen biedt de basis voor boekhoudkundige toegang tot en/of gebruik van gegevens door
    > verschillende gebruikers. Dit ondersteunt op zijn beurt belangrijke functies voor het wissen,
    > betalen en factureren (inclusief transacties voor het delen van gegevens zonder betrokkenheid
    > van gegevensmarktplaatsen).
  - **Publicatie- en marktplaatsdiensten**
    > Ter ondersteuning van het aanbod van gegevensbronnen en -diensten onder gedefinieerde algemene
    > voorwaarden, moeten marktplaatsen worden vastgesteld. Deze bouwsteen ondersteunt de publicatie
    > van deze aanbiedingen, het beheer van processen die gekoppeld zijn aan het maken en monitoren
    > van slimme contracten (die duidelijk de rechten en verplichtingen voor gegevens en
    > servicegebruik beschrijven) en toegang tot gegevens en diensten.
- **Vertrouwen**
  > Gegevenssoevereiniteit, met betrekking tot aspecten zoals identiteitsbeheer, betrouwbaarheid
  > van deelnemers, evenals gegevenstoegang en gebruikscontrole
  - **Identiteitsbeheer**
    > De IM -bouwsteen maakt identificatie, authenticatie en autorisatie mogelijk van
    > belanghebbenden die in een gegevensruimte actief zijn. Het zorgt ervoor dat organisaties,
    > individuen, machines en andere actoren worden voorzien van erkende identiteiten, en dat die
    > identiteiten kunnen worden geauthenticeerd en geverifieerd, inclusief aanvullende
    > informatievoorziening, kunnen worden gebruikt door autorisatiemechanismen om toegang en
    > gebruikscontrole mogelijk te maken. De IM -bouwsteen kan worden geïmplementeerd op basis van
    > direct beschikbare IM-platformen die delen van de vereiste functionaliteit behandelen.
    > Voorbeelden van open-source oplossingen zijn de Keycloak-infrastructuur, het Apache Syncope
    > IM-platform, het open-source IM-platform van het Shibboleth Consortium of het Fiware
    > IM-framework. Integratie van de IM -bouwsteen met de eID-bouwsteen van de Connecting Europe
    > Facility (CEF), ter ondersteuning van elektronische identificatie van gebruikers in heel
    > Europa, zou met name belangrijk zijn. Creatie van federale en vertrouwde identiteiten in
    > gegevensruimtes kan worden ondersteund door Europese voorschriften zoals EIDAS.
  - **Toegangs- en gebruikscontrole / beleid**
    > Deze bouwsteen garandeert de handhaving van gegevenstoegang en gebruiksbeleid gedefinieerd als
    > onderdeel van de algemene voorwaarden die zijn vastgesteld wanneer gegevensbronnen of
    > -diensten worden gepubliceerd (zie hieronder ‘Publication and Services Marketplace’ bouwsteen)
    > of onderhandeld tussen providers en consumenten. Een gegevensaanbieder implementeert doorgaans
    > mechanismen voor gegevenstoegang om misbruik van middelen te voorkomen, terwijl het
    > besturingsmechanismen voor gegevensgebruik meestal aan de gegevens van de gegevens van de
    > gegevens worden geïmplementeerd om misbruik van gegevens te voorkomen. In complexe
    > gegevenswaardeketens worden beide mechanismen gecombineerd door prosumeurs. Toegangscontrole
    > en gebruikscontrole vertrouwen op identificatie en authenticatie.
  - **Vertrouwde uitwisseling**
    > Deze bouwsteen faciliteert de vertrouwde gegevensuitwisseling onder deelnemers, waardoor
    > deelnemers in een gegevensuitwisselingstransactie geruststellen die andere deelnemers echt
    > zijn wie zij beweren te zijn en dat zij voldoen aan gedefinieerde regels/overeenkomsten. Dit
    > kan worden bereikt door organisatorische maatregelen (bijv. Certificering of geverifieerde
    > referenties) of technische maatregelen (bijv. Attesting op afstand).







### Mike's vertaling

- Governance
  - Bestuurlijk
  - Operationeel
  - Organisatie
- Interoperabiliteit
  - Modellen
  - Interoperabiliteit
  - Traceerbaarheid
- Data met waarde
  - Beschrijving
  - Verantwoording
  - Diensten
- Vertrouwen
  - Identiteit
  - Toegang
  - Veiligheid

FDS overwegingen:

- Enkel één woord is wel erg prettig!
- Enkel één woord geeft weinig 'lading' en verdient wel duidelijke beschrijvingen van wat er dan wel
  en niet onder valt. Hier ligt het gevaar dat 'men' alleen de woorden als uitgangspunt neemt en
  verschillende lading daar aan geeft. Hoewel een enkel woord eenvoudig lijkt en duidelijkheid lijkt
  te geven, zou het ook juist tot verwarring kunnen leiden. Dat is slechts een risico zo lang de
  woorden nog niet goed geladen zijn. Zodra dat wel het geval is, draagt het zeker bij aan een
  eenvoudig en duidelijk model!
- Interoperabiliteit > Interoperabiliteit ... is niet heel duidelijk. Zou de capability niet 'iets
  met API's' moeten bevatten ... alléén 'API's'?
- 'Data met waarde' is anders dan 'Data value'. Dat beschrijft meer de waarde _van_ data. De
  capabilities verwijzen meer naar de bekwaamheden die de data waarde gaan geven en/of duiden.
  Waarom niet directer vertalen met 'Data waarde'?
- Data ~~met~~ waarde > Beschrijving verwijst vooral naar de metadata. Is 'Metadata' beter dan
  'Beschrijving'?
- Data ~~met~~ waarde > Diensten gaat vooral over de publicatie en het gebruik van die publicatie in
  een marktplaats, terwijl diensten zelf ook kan duiden op de API's. Is 'Publicatie' beter?

### Peter's vertaling

- Governance
  - Standaard generieke samenwerkovereenkomst
  - Standaard operationele overeenkomst (SLA)
  - Regie op en continuïteit van het stelsel
- Interoperabiliteit
  - Datamodellen en -formaten
  - Datadeel services (API's)
  - Herkomst en traceerbaarheid
- Waarde van data
  - Protocol voor metadata en zoeken
  - Verantwoording data gebruik
  - Services voor datapublicatie en datamarktplaats
- Vertrouwen
  - Identiteit management
  - Beleid toegang en gebruik
  - Vertrouwd data delen

### Capabilities sessies

- Governance
  - [nog consolideren]
- Interoperabiliteit
  - Datamodellen en formaten
  - Data uitwisseling en APIs
  - Herleidbaarheid en traceerbaarheid
  - [extra] Ontwikkelen en beheren meta-data-standaarden
- Data waarde
  - Metadata en discovery
  - Verantwoording datagebruik
  - Publicatie en marktplaats-services
- Vertrouwen
  - Identiteitsbeheer
  - Toegangs- en gebruikerscontrole/beleid
  - Vertrouwd uitwisselen

FDS overwegingen:

Tijdens de capabilities sessies zijn we steeds afgeweken van de OpenDEI building blocks. Dat heeft
geleid tot goede discussies en uitwerking van diverse (bijv.) rollen van het federatief datastelsel.
Echter ... wijkt dat dusdanig af dat het niet meer herkenbaar is naar het oorspronkelijke model van
OpenDEI. Aangezien we in DR [00001 Basisstructuur](./00001-basisstructuur/) hebben besloten om juist
wél dicht bij het OpenDEI model te blijven, moeten we herzien wat we tijdens de sessies bedacht
hebben.

### Vergelijkingstabel

| OpenDEI oorspronkelijk               | OpenDEI letterlijke vertaling          | Mike's vertaling   | Peter's vertaling                               | Capabilities sessies                                 |
| ------------------------------------ | -------------------------------------- | ------------------ | ----------------------------------------------- | ---------------------------------------------------- |
| Governance                           | Bestuur                                | Governance         | Governance                                      | Governance                                           |
| Overarching cooperation agreement    | Overkoepelend samenwerkingsakkoord     | Bestuurlijk        | Standaard generieke samenwerkovereenkomst       | [nog consolideren]                                   |
| Operational (e.g. SLA)               | Operationeel (bijv. SLA)               | Operationeel       | Standaard operationele overeenkomst (SLA)       |
| Continuity model                     | Continuïteitsmodel                     | Organisatie        | Regie op en continuïteit van het stelsel        |
| Interoperability                     | Interoperabiliteit                     | Interoperabiliteit | Interoperabiliteit                              | Interoperabiliteit                                   |
| Data models and formats              | Datamodellen en formaten               | Modellen           | Datamodellen en -formaten                       | Datamodellen en formaten                             |
| Data exchange APIs                   | API's voor gegevensuitwisseling        | Interoperabiliteit | Datadeel services (API's)                       | Data uitwisseling en APIs                            |
| Provenance and traceability          | Herkomst en traceerbaarheid            | Traceerbaarheid    | Herkomst en traceerbaarheid                     | Herleidbaarheid en traceerbaarheid                   |
|                                      |                                        |                    |                                                 | [extra] Ontwikkelen en beheren meta-data-standaarden |
| Data value                           | Gegevenswaarde                         | Data met waarde    | Waarde van data                                 | Data waarde                                          |
| Metadata and Discovery Protocol      | Metagegevens en ontdekkingsprotocol    | Beschrijving       | Protocol voor metadata en zoeken                | Metadata en discovery                                |
| Data usage accounting                | Accounting van gegevensgebruik         | Verantwoording     | Verantwoording data gebruik                     | Verantwoording datagebruik                           |
| Publication and marketplace services | Publicatie- en marktplaatsdiensten     | Diensten           | Services voor datapublicatie en datamarktplaats | Publicatie en marktplaats-services                   |
| Trust                                | Vertrouwen                             | Vertrouwen         | Vertrouwen                                      | Vertrouwen                                           |
| Identity management                  | Identiteitsbeheer                      | Identiteit         | Identiteit management                           | Identiteitsbeheer                                    |
| Access and usage control / policies  | Toegangs- en gebruikscontrole / beleid | Toegang            | Beleid toegang en gebruik                       | Toegangs- en gebruikerscontrole/beleid               |
| Trusted exchange                     | Vertrouwde uitwisseling                | Veiligheid         | Vertrouwd data delen                            | Vertrouwd uitwisselen                                |

## Links <!-- optional -->

- DR [00001 Basisstructuur](./00001-basisstructuur/)
- doc [Raamwerk voor ontwikkeling in samenwerking](/docs/realisatie/raamwerk/)
- doc [Capabilities](/docs/realisatie/capabilities/)