---
title: Contributie
weight: 10
description: >
  Voel jij je aangesproken en wil je meteen al actief bijdragen? Hier lees je hoe dat kan
---

<a class="btn btn-lg btn-primary me-3 mb-4" href="/docs/welkom/">
  Welkom <i class="fas fa-arrow-alt-circle-right ms-2"></i>
</a>
<a class="btn btn-lg btn-primary me-3 mb-4" href="/docs/welkom/werkomgeving/">
  Werkomgeving <i class="fas fa-arrow-alt-circle-right ms-2"></i>
</a>
<a class="btn btn-lg btn-secondary me-3 mb-4" href="https://digilab.overheid.nl/chat/fds" target="_blank">
  Mattermost <i class="fas fa-comments ms-2"></i>
</a>
<a class="btn btn-lg btn-secondary me-3 mb-4" href="https://gitlab.com/datastelsel.nl/federatief/website" target="_blank">
  Sources <i class="fab fa-gitlab ms-2 "></i>
</a>

Bovenstaande links zijn goede startpunten, afhankelijk van wat je zoekt en/of nodig hebt.

Contributies zijn zeer welkom! Er zijn verschillende vormen van bijdragen en betrokkenheid. Om te
beginnen is het mogelijk om via chat vragen te stellen en/of mee te discussiëren. Dat kan via
[Mattermost](#mattermost). Daarnaast is het mogelijk om issues aan te maken die gerelateerd zijn aan
de content van deze site. Dat kan in GitLab. Daar wordt ook de bron van de content mbv versiebeheer
ontwikkeld en dat is gelijk de mooiste manier van bijdragen: een [merge request] met daarin je
voorstel voor wijzigingen en/of bijdragen! Hopelijk biedt onderstaande hoofdstukjes voldoende
houvast om bijdragen te kunnen doen en anders horen we graag hoe we dat beter kunnen maken 😃

## Mattermost

Voor chat functionaliteit maken we gebruik van Mattermost. We liften mee op de ontwikkelingen van <a
href="https://digilab.overheid.nl/" target="_blank">Digilab</a> en gebruik dan ook hun Mattermost
omgeving. In Mattermost is een 'Federatief Datastelsel' 'team' (Mattermost jargon) waar alle
federatief datastelsel vragen en discussies plaatsvinden. Deze is te vinden op: <a
href="https://digilab.overheid.nl/chat/fds" target="_blank">digilab.overheid.nl/chat/fds</a>.

Het kanaal <a href="https://digilab.overheid.nl/chat/fds/channels/town-square"
target="_blank">~algemeen</a> is het startpunt van alle vragen en discussies!

## GitLab

Voor versiebeheer maken we gebruik van Git. Dit is hét versiebeheersysteem van dit moment ... en al
voor jaren! We hebben gekozen voor GitLab als platform om mee te werken. De content van deze website
is te vinden op: <a href="https://gitlab.com/datastelsel.nl/federatief/website"
target="_blank">gitlab.com/datastelsel.nl/federatief/website</a>

### Issues

GitLab ondersteunt issues. 'Issues' is het jargon voor 'alles wat maar een taakje zou kunnen zijn'.
Het duidt dus niet alleen maar op problemen, maar op vragen, verzoeken, uitzoekwerk, etc, etc.

In het <a href="https://gitlab.com/datastelsel.nl/federatief/website/-/boards" target="_blank">issue
board</a> kun je nieuwe issues aanmaken met je wensen, vragen en opmerkingen. Maak deze aan in de
kolom 'Open' door op het ![GitLab Issue Board Plus Sign](images/gitlab-issues-plus.png) te klikken.

### Merge request

Heuse bijdragen aan de content van deze site kan met behulp van een 'merge request'. Daarvoor is het
wel noodzakelijk om begrip te hebben van hoe Git werkt en hoe je een merge request kunt aanmaken.

![Git Branching](images/git-branching.jpg)

![Git Repository Forking visualization](images/git-repo-forking.png)

Een Git repository bevat een collectie bestanden die, in dit geval, de content vormen van deze site.
De 'Core Committers' van deze repo zijn (op dit moment) van het team IBDS/FDS. Je kunt in de groep
van 'Contributors' komen, door een `fork` te maken onder je eigen naam of organisatie (in GitLab).
Een fork is een apart soort branch onder een andere organisatie dan de originele repository. Daar
ben jij dan 'Core Committer' van en kun je je wijzigingen doen dmv Git commits. Vervolgens kun je
een 'Merge Request' aanmaken terug naar de originele repository. Deze wordt dan zichtbaar bij de
'Core Committers' van federatief.datastelsel.nl en zij kunnen deze accepteren. Accepteren betekent
dat de wijzigingen worden opgenomen in de repository van deze website. Daarop volgt een update van
de website met de nieuwe content. Voorafgaande aan het accepteren wordt er eerst een review gedaan
door de 'Core Committers' en kan eventueel commentaar en discussie volgen over de bijdragen.

Als je je [bijdrage maken](#bijdrage-maken) hebt afgerond, kun je via je browser navigeren naar de
repository in GitLab. Afhankelijk van waar je branch staat, bijvoorbeeld in je fork, geeft GitLab je
al een hint om een Merge Request te maken:

![GitLab Create Merge Request tip](images/gitlab-create-merge-request-tip.png)

Een andere manier is om je branch te bekijken en te vergelijken ('compare') met het origineel (of de
`main` branch). Daar biedt GitLab je vervolgens ook de mogelijkheid om een Merge Request aan te
maken:

![GitLab Branches New Merge Request](images/gitlab-branches-new-merge-request.png)

### Online werken

Om online contributies te doen is het mogelijk om de 'Web IDE' te openen. IDE staat voor Integrated Development Environment. Dat klinkt technisch ... en dat is het wellicht ook wel een beetje. Tegelijk hoeft je niet alle mogelijkheden te gebruiken en is het ook 'gewoon een editor' 😉

Om de 'Web IDE' te openen navigeer je eerst naar de repository in GitLab: 

Vervolgens klik je op Edit > Web IDE

![GitLab Web IDE](images/gitlab-webide.png)

Vervolgens wordt er een online versie van VSCode geopend waarin wijzigingen gedaan kunnen worden.
Volg daarna de instructie van [bijdrage maken](#bijdrage-maken).

### Offline werken

Om offline te kunnen werken en niet per se een internetverbinding nodig te hebben, is het mogelijk om op je computer, lokaal, een kopie van de repository te maken. Daarvoor maken we ook gebruik van Git functionaliteiten. Eigenlijk lijkt dat op het maken van een `fork` (zie bovenstaande) maar dan maak je een lokale kopie. Dat heet (Git jargon) een `clone`.

#### Installatie

Voorwaarden om lokaal op je eigen computer te kunnen werken, is beschikbaarheid van een aantal geïnstalleerde tools:

1. [VSCode](https://code.visualstudio.com/) (Visual Studio Code - Microsoft)
1. [Git-scm](https://git-scm.com/) (Source Code Management - open source)
   
   > (kies bij de installatie voor alle 'defaults' behalve `default editor`; hier kies je voor `Use Visual Studio Code as Git's Default Editor`)

#### Configuratie

Er is enige configuratie nodig om optimaal te kunnen werken. Volg deze stappen:

1. Open VSCode (na installatie van Git-scm)
1. Open een `terminal` om Git te configureren (klinkt eng, maar valt reuze mee 😉 - zie ook <a
   href="https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#configure-git"
   target="_blank">GitLab Basics documentatie</a>)
1. Type het volgende commando in (copy-paste mag, maar pas aan naar je eigen gegevens)
   
   ```git config --global user.name "Mona Lisa"```

1. Type het volgende commando in (copy-paste mag, maar pas aan naar je eigen gegevens)
   
   ```git config --global user.email "mona.lisa@mail.com"```

1. Sluit de terminal af met het commando:
   
   ```exit```

#### Clone

We zijn nu klaar om een lokale kopie te kunnen maken. GitLab biedt daarvoor een eenvoudige optie:

1. Navigeer naar de repository in GitLab: <a href="https://gitlab.com/datastelsel.nl/federatief/website"
target="_blank">gitlab.com/datastelsel.nl/federatief/website</a>
1. Klik op Code > Visual Studio Code (HTTPS)
   
   ![GitLab Code VSCode HTTPS](images/gitlab-code-vscode-https.png)

1. VSCode wordt geopend en je krijgt de vraag waar de lokale kopie op je computer opgeslagen moet
   worden. Hier dien je een map op te geven waarin Git voor jou een map aanmaakt die heet `website`.
   Dat komt omdat de repository (het laatste stukje) zo heet (Git default; is aanpaspaar ... maar
   vraag extra Git kennis)

   Gebruikelijk is een map waarin alle lokale repositories staan, bijvoorbeeld: `C:\repos`

   > <span class="fas fa-warning"></span> _NOTE Een bekend probleem is lange bestandsnamen. Kies
   daarom een map die niet al heel erg diep genest is en een moeilijke naam heeft. Dat zorgt nog wel
   eens voor onverklaarbare problemen._

1. Nadat je de map gekozen hebt _waarin_ Git voor jou een kopie maakt van de repository, de 'clone',
   opent VSCode deze direct en kun je aan de slag om je bijdragen te maken. Volg daarin dezelfde
   werkwijze als het [online werken](#online-werken): [bijdrage maken](#bijdrage-maken).

### Bijdrage maken

Voordat je begint met het maken van wijzigingen, is het van belang om een `branch` te maken. Dit is
Git functionaliteit om zonder een ander tot last te zijn en om wijzigingen goed te kunnen beheren,
op een 'kopie' te werken. Een branch biedt de mogelijkheid om wijzigingen te maken in meerdere
bestanden en met meerdere `commit`s, deze te verzamelen en als geheel te reviewen en te accepteren.

In VSCode zie je continue in welke branch je zit, links onderaan in je scherm. Door daarop te
klikken kun je kiezen om een andere branch te selecteren en/of om een nieuwe branch te maken. Het is
raadzaam om te beginnen met een nieuwe branch _voordat_ je wijzigingen maakt.

> <span class="fas fa-warning"></span> _Git kent vele mogelijkheden en manieren om versiebeheer toe
> te passen en het is bijna moeilijk om wijzigingen kwijt te raken. Tegelijk kan dat ook bijzonder
> complex worden. Het is daarom raadzaam om gestructureerd en methodisch te werken. Voor problemen
> met Git en/of wijzigingen kun je altijd hulp vragen in [Mattermost](#mattermost)_

Om een bijdrage te maken is het nodig om te weten _waar_ je een bijdrage wilt gaan maken. Je wilt
een nieuw onderwerp toevoegen of een wijzigingsvoorstel maken op bestaande pagina's. Vrijwel alle
inhoudelijke wijzigingen en bijdragen bevinden zich in de map **`content`**. Dat is een goed begin
om te gaan zoeken naar de pagina waarin je een bijdrage zou willen maken. 

Navigeer aan de linkerkant in VSCode door de 'Explorer' om de juiste pagina te vinden. Door daarop
te klikken, wordt de pagina geopend. Dit is Markdown. Gebruik
[Markdown](/docs/community/hulp/markdown/) opmaak symbolen om je wijzigingen te maken.

Plaatjes zijn eenvoudig toe te voegen door deze in een submapje `images` te kopiëren of plakken. Het
is ook mogelijk om op de plek waar je een plaatje wilt invoegen in de Markdown, gewoon een plaatje
te plakken van het klembord (Ctrl+v). VSCode (zowel online als offline) vertaalt dit naar een
`image.png` en voegt passende Markdown opmaak toe om het plaatje weer te geven.

VSCode biedt de mogelijkheid om een preview van de Markdown pagina direct weer te geven binnen
VSCode. Er zijn meerdere manieren om deze weer te geven. Kies één van de volgende opties:

- Klik op de 'preview button' rechts bovenaan ![VSCode Preview button](images/vscode-preview.png)
- Gebruik de shortcut `Ctrl + k` `v` (dus eerst `Ctrl + k` en daarna een 'losse' `v`)
- Open het 'Command Palette' van VSCode met de shortcut `Ctrl + Shift + P` en type het gewenste
  command (bijv. begin met typen van `preview` en selecteer _Markdown: Open **Preview** to the
  side_)

Nadat je alle gewenste wijzigingen hebt gemaakt, dat kan in meerdere bestanden zijn, dien je al je
wijzigingen te bewaren met een 'Git `commit`' (Git jargon). Een commit wordt in bovenstaande
plaatjes aangeduid met een bolletje. Het bevat een verzameling van wijzigingen in één of meerdere
bestanden. Daarvoor wil je eerst zien wat je wijzigingen zijn en deze zelf reviewen. VSCode maakt
dat gemakkelijk zichtbaar in het 'Source Control' window:

![VSCode Source Control](images/vscode-source-control.png)

1. Selecteer _Source Control_ (of gebruik shortcut `Ctrl + Shift + G`)
1. Hier zie je een overzicht van alle gewijzigde bestanden. Tijd voor een persoonlijke review!
   - Is dit wat je verwacht?
   - Je kunt elk bestand aanklikken om te zien wat je wijzigingen zijn
   - Maak eventueel meer wijzigingen om te komen tot je gewenste resultaat
1. Het is gebruikelijk en voor de traceerbaarheid prettig om enig commentaar te maken bij je
   verzameling wijzigingen, oftewel je 'commit message'
   - Beschrijf in 't kort (bij voorkeur binnen 50 karakters) waarom je deze wijzigingen hebt gemaakt
   - Eventueel kun je een korte zin gebruiken en vervolgens met `enter` uitgebreidere commentaren en
     opmerkingen maken
     
     > <span class="fas fa-warning"></span> _NOTE Ook hier is Markdown opmaak te gebruiken_

1. [optioneel] Git en VSCode ondersteunen dat je wijzigingen in verschillende commits doet. Daarvoor
   kun je in het lijstje van bestanden kiezen welke bestanden je mee wilt nemen in je commit. Klik
   daarvoor op de `+` van elk bestand dat je mee wilt nemen.
   
   > <span class="fas fa-warning"></span> _NOTE Het is mogelijk om deze stap over te slaan. VSCode
   > vraagt dan of alle gewijzigde bestanden meegenomen moeten worden in deze commit. Vaak is dat
   > het geval en wordt deze stap dus overgeslagen. Gemak dient de mens_ 😄
   
1. Commit je wijzigingen door op de **Commit** knop te klikken
1. [extra voor **offline** werken!] In het geval dat je offline werkt, heb je nu een lokale `branch`
   gemaakt ... maar deze is nog niet beschikbaar of bijgewerkt in GitLab. Daarvoor dien je je lokale
   repository (je `clone` dus) te synchroniseren met de online repository. In VSCode heet dat
   **Synchonize** maar wat er in werkelijkheid gebeurt, zijn twee stappen: een `git pull` en `git
   push` wat resp. de Git commando's zijn voor het ophalen van wijzigingen en uploaden (wegduwen)
   van wijzigingen. In deze zijn de wijzigingen 'de commits'.

Nadat je wijzigingen in een commit zijn vastgelegd én je commits zijn beschikbaar in GitLab, dan is
het mogelijk om een [Merge Request](#merge-request) te maken 💪
