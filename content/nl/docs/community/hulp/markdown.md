---
title: Markdown
weight: 90
description: >
  Wat is Markdown? Wat waren de opmaak codes ook alweer?
---

> Markdown is een lichtgewicht opmaaktaal op basis van platte tekst die zodanig ontworpen is dat het gemakkelijk valt te converteren naar HTML en andere formaten met een applicatie met dezelfde naam.
> Markdown wordt vaak gebruikt voor de opmaak van projectdocumentatie, eenvoudige CMS-systemen en berichten in online fora. - bron: [Wikipedia](https://nl.wikipedia.org/wiki/Markdown)

Markdown is platte tekst en daardoor ook goed machine-leesbaar.
Dat heeft voordelen om meerdere formaten te produceren van dezelfde content.
De opmaak wordt door 'speciale opmaaktaal' opgegeven _in_ de tekst.
In het genereren van een website of PDF worden die opmaak 'codes' omgezet in de juiste opmaak.

Doordat Markdown een open standaard is,
zijn er vele editors en tools die kunnen ondersteunen bij het werken met Markdown.
In alle gevallen is het handig om een klein beetje van de opmaaktaal te leren.
Hiervoor zijn vele 'Markdown cheat sheets' te vinden ... en hieronder staat onze eigen 😄

Waarom eigenlijk Markdown? Kijk daarvoor in onze blogs over [strategie van
samenwerken](/docs/welkom/strategie-van-samenwerken/) en onze
[werkomgeving](/docs/welkom/werkomgeving/).

## Basic Syntax

| Element                                                               | Markdown Syntax                                                                           | Voorbeeld[^1]                          |
| --------------------------------------------------------------------- | ----------------------------------------------------------------------------------------- | -------------------------------------- |
| <span id="heading">[Heading](#heading)</span>                         | \# H1<br>\#\# H2<br>\#\#\# H3                                                             | -                                      |
| <span id="bold">[Bold](#bold)</span>                                  | \*\*bold\*\*                                                                              | **bold**                               |
| <span id="italic">[Italic](#italic)</span>                            | \*italic\*<br>\_italic\_                                                                  | _italic_                               |
| <span id="blockquote">[Blockquote](#blockquote)</span>                | \> blockquote                                                                             | -                                      |
| <span id="ordered-list">[Ordered list](#ordered-list)</span>          | 1. item 1<br>2. item 2<br>3. item 3<br>(alleen 1. mag ook; wordt automatisch genummerd ;) | 1. item 1<br>2. item 2<br>3. item 3    |
| <span id="unordered-list">[Unordered list](#unordered-list)</span>    | - item 1<br>- item 2<br>- item 3                                                          | - item 1<br>- item 2<br>- item 3       |
| <span id="code">[Code](#code)</span>                                  | \`code\`                                                                                  | `code`                                 |
| <span id="link">[Link](#link)</span> [^2]                             | \[label\]\(<https://datastelsel.nl\>)                                                     | [label](https://datastelsel.nl)        |
| <span id="image">[Image](#image)</span>                               | !\[alt text\]\(../images/linked-data.png\)                                                | ![alt text](../images/linked-data.png) |
| <span id="horizontal-rule">[Horizontal rule](#horizontal-rule)</span> | \-\-\-                                                                                    | -                                      |
| <span id="footnote">[Footnote](#footnote)</span>                      | In de tekst: \[^3\]<br> en dan onderaan de pagina: <br>\[^3\]: _de footnote zelf_         | [^3]                                   |

[^1]: Mits mogelijk om weer te geven in een tabel

[^2]: links naar interne pagina's moeten 'relatief' zijn. Dat betekent `../` voor een folder omhoog,
    `./` voor in dezelfde folder en de namen van de folders gewoon zoals ze zijn.

[^3]: Voorbeeld van een footnote (_er komt automatisch een linkje terug naar waar de footnote in de
    tekst staat_)
